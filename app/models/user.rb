class User < ApplicationRecord
    before_save { email.downcase! }
  
    validates :name, presence: true, length: { maximum: 50 }
  
    validates :email, presence: true, length: { maximum: 255 }
    #VALID_EMAIL_REGEX = /\A[\w+\-.]+@[a-z\d\-.]+\.[a-z]+\z/i
    VALID_EMAIL_REGEX = /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i
    validates :email, format: { with: VALID_EMAIL_REGEX, on: :create },
                      uniqueness: true
  
    has_secure_password
    validates :password, presence: true, length: { minimum: 6 }
  end
  