require "test_helper"

class ApplicationHelperTest < ActionView::TestCase
    test "full title helper" do # Unit Test für helper-Methode
        assert_equal full_title, "Ruby on Rails Sample App"
        assert_equal full_title("Help"), "Help | Ruby on Rails Sample App"
    end
end