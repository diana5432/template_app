require 'test_helper'

class SiteLayoutTest < ActionDispatch::IntegrationTest
  test "layout links" do
    get root_url # wenn die Seite gerendert wird
    assert_template "home/index" # erwarte dass views/home/index.html.slim gerendert wird
    assert_select "a[href=?]", root_path, count: 2 # wie jQuery Selector, d.h. soll 2mal da sein
    assert_select "a[href=?]", page_url("help")  # d.h. es soll ein link nach about da sein
    assert_select "a[href=?]", page_url("about")
    assert_select "a[href=?]", page_url("contact")
    get page_url("contact")
    assert_template "pages/contact"
    assert_select "title", full_title("Contact")
  end
end
