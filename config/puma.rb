# if ENV['RAILS_ENV'] == 'production'
#   app_root = '/data/www/template_app/shared'
#   pidfile "#{app_root}/tmp/pids/puma.pid"
#   state_path "#{app_root}/tmp/pids/puma.state"
#   bind "unix://#{app_root}/tmp/sockets/puma.sock"
#   activate_control_app "unix://#{app_root}/tmp/sockets/pumactl.sock"
#   daemonize true
#   workers 2
#   threads 8, 16
#   prune_bundler

#   stdout_redirect "#{app_root}/log/puma_access.log", "#{app_root}/log/puma_error.log", true
# else
#   plugin :tmp_restart
# end

workers Integer(ENV['WEB_CONCURRENCY'] || 2)
threads_count = Integer(ENV['RAILS_MAX_THREADS'] || 5)
threads threads_count, threads_count

preload_app!

rackup      DefaultRackup
port        ENV['PORT']     || 3000
environment ENV['RACK_ENV'] || 'development'

on_worker_boot do
  # Worker specific setup for Rails 4.1+
  # See: https://devcenter.heroku.com/articles/deploying-rails-applications-with-the-puma-web-server#on-worker-boot
  ActiveRecord::Base.establish_connection
end
